#!/bin/bash

  # 1. compile

  g++ -O3                              \
      -Wall                            \
      -std=gnu++14                     \
	  -static                          \
      driver_program.cpp               \
	  /opt/fftw/334/gnu/lib/libfftw3.a \
      -o x_gnu
