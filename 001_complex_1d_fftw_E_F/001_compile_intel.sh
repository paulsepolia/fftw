#!/bin/bash

  # 1. compile

  icpc -O3                                \
       -xHost                             \
       -Wall                              \
       -std=c++11                         \
       -wd2012                            \
       -static                            \
       driver_program.cpp                 \
       /opt/fftw/334/intel/lib/libfftw3.a \
       -o x_intel
