
//=============================================================//
//  complex 1d ftw transform				       //	
//  fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE); //                                                
//=============================================================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <fftw3.h> 

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;

// the main function

int main()
{
	// local variables and parameters 

	const long N = static_cast<long>(pow(10.0, 8.0));
	const int K_MAX = static_cast<int>(pow(10.0, 5.0));

	// adjust the output format

	cout << fixed;
	cout << setprecision(20);
	cout << showpos;
	cout << showpoint;

	for (int k = 0; k != K_MAX; ++k)
	{
		cout << "------------------------------------------------------------------->> " << k << endl;

		fftw_complex * in;
		fftw_complex * out;
		fftw_plan p;

		// allocate space

		cout << " -->  1 --> allocate RAM space for input data" << endl;

		in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

		cout << " -->  2 --> allocate RAM space for output data" << endl;

		out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

		cout << " -->  3 --> create the fftw plan" << endl;

		p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

		cout << " -->  4 --> build the complex input data vector" << endl;

		// build the "in" complex data item
		// it is "double[N][2]"
		// so the real part is "in[i][0]"
		// and the imaginary part is "in[i][1]"

		// build the real part

		cout << " -->  5 --> build the real part of the complex input data vector" << endl;

		for (long i = 0; i != N; ++i)
		{
			in[i][0] = cos(static_cast<double>(i));
		}

		// build the imaginary part

		cout << " -->  6 --> build the imaginary part of the complex input data vector" << endl;

		for (long i = 0; i != N; ++i)
		{
			in[i][1] = sin(static_cast<double>(i));
		}

		// execute the fftw calculation

		cout << " -->  7 --> execute the fftw transformation" << endl; 

		fftw_execute(p);

		cout << " -->  8 --> some output" << endl;

		cout << " -->  out[0][0]   = " << out[0][0] << endl;
		cout << " -->  out[1][0]   = " << out[1][0] << endl;
		cout << " -->  out[2][0]   = " << out[2][0] << endl;
		cout << " -->  out[3][0]   = " << out[3][0] << endl;
		cout << " -->  out[N-1][0] = " << out[N-1][0] << endl;
	
		cout << " -->  out[0][1]   = " << out[0][1] << endl;
		cout << " -->  out[1][1]   = " << out[1][1] << endl;
		cout << " -->  out[2][1]   = " << out[2][1] << endl;
		cout << " -->  out[3][1]   = " << out[3][1] << endl;
		cout << " -->  out[N-1][1] = " << out[N-1][1] << endl;
	
		cout << " -->  9 --> destroy plan" << endl;

		fftw_destroy_plan(p);

		cout << " --> 10 --> delete RAM of input data" << endl;

		fftw_free(in);

		cout << " --> 11 --> delete RAM of output data" << endl;

		fftw_free(out);

		cout << " --> 12 --> repeat" << endl;
	}

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

