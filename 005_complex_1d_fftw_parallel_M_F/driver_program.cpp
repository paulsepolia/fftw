
//=============================================================//
//  complex 1d ftw transform				       //	
//  fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE); //                                                
//  openmp version 					       //
//  using "new" to allocate RAM                                //
//  and the std::complex class 				       //
//=============================================================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <complex>
#include <fftw3.h> 

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::clock;
using std::complex;

// the main function

int main()
{
	// local variables and parameters 

	const long N = 1.2 * static_cast<long>(pow(10.0, 8.0));
	const int K_MAX = static_cast<int>(pow(10.0, 5.0));
	const int nt = 4;
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << setprecision(20);
	cout << showpos;
	cout << showpoint;

	for (int k = 0; k != K_MAX; ++k)
	{
		cout << "------------------------------------------------------------------->> " << k << endl;

		cout << " -->  1 --> initialize the openmp fftw environment" << endl;

	        fftw_init_threads();

		complex<double> * in;
		complex<double> * out;
		fftw_plan p;

		// allocate space

		cout << " -->  2 --> allocate RAM space for input data" << endl;

		in = new complex<double> [N];

		cout << " -->  3 --> allocate RAM space for output data" << endl;

		out = new complex<double> [N];

		cout << " -->  4 --> parallelize any subsequent plan" << endl;

		fftw_plan_with_nthreads(nt);

		cout << " -->  5 --> create the fftw plan" << endl;

		p = fftw_plan_dft_1d(N, reinterpret_cast<fftw_complex*>(in), 
					reinterpret_cast<fftw_complex*>(out), 
					FFTW_FORWARD, FFTW_ESTIMATE);

		cout << " -->  6 --> build the complex input data vector" << endl;

		#pragma omp parallel default (none) \
				     shared (in)
		{
			#pragma omp for
			for (long i = 0; i < N; ++i)
			{
				in[i] = complex<double>(
						cos(static_cast<double>(i)), 
						sin(static_cast<double>(i)));
			}
		}

		// execute the fftw calculation

		cout << " -->  7 --> execute the fftw transformation" << endl; 

		t1 = clock();

		fftw_execute(p);

		t2 = clock();

		cout << " -->  time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

		cout << " -->  8 --> some output" << endl;

		cout << " -->  out[0].real()   = " << out[0].real() << endl;
		cout << " -->  out[1].real()   = " << out[1].real() << endl;
		cout << " -->  out[2].real()   = " << out[2].real() << endl;
		cout << " -->  out[3].real()   = " << out[3].real() << endl;
		cout << " -->  out[N-1].real() = " << out[N-1].real() << endl;
	
		cout << " -->  out[0].imag()   = " << out[0].imag() << endl;
		cout << " -->  out[1].imag()   = " << out[1].imag() << endl;
		cout << " -->  out[2].imag()   = " << out[2].imag() << endl;
		cout << " -->  out[3].imag()   = " << out[3].imag() << endl;
		cout << " -->  out[N-1].imag() = " << out[N-1].imag() << endl;
	
		cout << " -->  9 --> destroy plan" << endl;

		fftw_destroy_plan(p);

		cout << " --> 10 --> cleanup fftw threads" << endl;

		fftw_cleanup_threads();

		cout << " --> 11 --> delete RAM of input data" << endl;

		delete [] in;

		cout << " --> 12 --> delete RAM of output data" << endl;

		delete [] out;

		cout << " --> 13 --> repeat" << endl;
	}

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

