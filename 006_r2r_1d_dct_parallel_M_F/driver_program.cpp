
//========================================================//
//  r2r 1d fftw transform				  //
//  fftw_plan_r2r_1d(int n, double * in, double * out,    //
//                   fftw_r2r_kind kind, unsigned flags); //
//  openmp version 					  //
//  using "new" to allocate RAM                           //
//========================================================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <fftw3.h> 

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::clock;

// the main function

int main()
{
	// local variables and parameters 

	const long N = 1.0 * static_cast<long>(pow(10.0, 8.0));
	const int K_MAX = static_cast<int>(pow(10.0, 5.0));
	const int nt = 4;
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << setprecision(20);
	cout << showpos;
	cout << showpoint;

	for (int k = 0; k != K_MAX; ++k)
	{
		cout << "------------------------------------------------------------------->> " << k << endl;

		cout << " -->  1 --> initialize the openmp fftw environment" << endl;

	        fftw_init_threads();

	        double * in;
		double * out;
		fftw_plan p;

		// allocate space

		cout << " -->  2 --> allocate RAM space for input data" << endl;

		in = new double [N];

		cout << " -->  3 --> allocate RAM space for output data" << endl;

		out = new double [N];

		cout << " -->  4 --> parallelize any subsequent plan" << endl;

		fftw_plan_with_nthreads(nt);

		cout << " -->  5 --> create the fftw plan" << endl;

		p = fftw_plan_r2r_1d(N, in, out, FFTW_REDFT10, FFTW_ESTIMATE);

		// build the input data vector

		cout << " -->  6 --> build the input data vector" << endl;

		#pragma omp parallel default (none) \
				     shared (in)
		{
			#pragma omp for
			for (long i = 0; i < N; ++i)
			{
				in[i] = cos(static_cast<double>(i));
			}
		}

		// execute the fftw calculation

		cout << " -->  7 --> execute the fftw transformation" << endl; 

		t1 = clock();

		fftw_execute(p);

		t2 = clock();

		cout << " -->  time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

		cout << " -->  8 --> some output" << endl;

		cout << " -->  out[0]   = " << out[0] << endl;
		cout << " -->  out[1]   = " << out[1] << endl;
		cout << " -->  out[2]   = " << out[2] << endl;
		cout << " -->  out[3]   = " << out[3] << endl;
		cout << " -->  out[N-1] = " << out[N-1] << endl;
	
		cout << " -->  9 --> destroy plan" << endl;

		fftw_destroy_plan(p);

		cout << " --> 10 --> cleanup fftw threads" << endl;

		fftw_cleanup_threads();

		cout << " --> 11 --> delete RAM of input data" << endl;

		delete [] in;

		cout << " --> 12 --> delete RAM of output data" << endl;

		delete [] out;

		cout << " --> 13 --> repeat" << endl;
	}

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

