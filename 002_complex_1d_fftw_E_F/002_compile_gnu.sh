#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=c++0x         \
       -static            \
       driver_program.cpp \
       -lfftw3            \
       -lm                \
       -o x_gnu
