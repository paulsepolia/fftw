#!/bin/bash

  # 1. compile

  icpc  -O3                \
        -xHost             \
        -Wall              \
        -std=c++11         \
        -wd2012            \
        -static            \
        driver_program.cpp \
        -lfftw3            \
        -lm                \
        -o x_intel
