#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=c++0x         \
       -static            \
       -fopenmp           \
       -lpthread          \
       driver_program.cpp \
       /opt/fftw/334/gnu_ubu/lib/libfftw3_omp.a     \
       /opt/fftw/334/gnu_ubu/lib/libfftw3.a         \
       /opt/fftw/334/gnu_ubu/lib/libfftw3_threads.a \
       -o x_gnu
