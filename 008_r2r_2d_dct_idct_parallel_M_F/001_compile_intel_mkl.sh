#!/bin/bash

  # 1. compile

  icpc  -O3                \
        -xHost             \
        -Wall              \
        -std=c++11         \
        -wd2012            \
	driver_program.cpp \
	-parallel          \
	-par-threshold0    \
	-static            \
        -mkl=parallel      \
        -openmp            \
        -lpthread          \
        -o x_intel_mkl
