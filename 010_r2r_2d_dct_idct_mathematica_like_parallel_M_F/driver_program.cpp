
//==============================================//
//  r2r 1d fftw transform			//
//  fftw_plan_r2r_2d(int n0, int n1, 		//
//                   double * in, double * out, //
//                   fftw_r2r_kind kind0,       //
//                   fftw_r2r_kind_kind1,       //
//                   unsigned flags); 		//
//  openmp version 				//
//  using "new" to allocate RAM                 //
//==============================================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <fftw3.h> 

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::clock;

// the main function

int main()
{
	// local variables and parameters 

	const long N = 1.0 * static_cast<long>(pow(10.0, 2.0));
	const int K_MAX = static_cast<int>(pow(10.0, 5.0));
	const int nt = 1;
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << setprecision(4);
	cout << showpos;
	cout << showpoint;

	for (int k = 0; k != K_MAX; ++k)
	{
		cout << "------------------------------------------------------------------->> " << k << endl;

		// initailize parallel execution of fftw

		cout << " -->  1 --> initialize the openmp fftw environment" << endl;

	        fftw_init_threads();

	        double * in;
		double * out;
		fftw_plan p;

		// allocate space

		cout << " -->  2 --> allocate RAM space for input data" << endl;

		in = new double [N*N];

		cout << " -->  3 --> allocate RAM space for output data" << endl;

		out = new double [N*N];

		// parallelize using specific number of threads

		cout << " -->  4 --> parallelize any subsequent plan" << endl;

		fftw_plan_with_nthreads(nt);

		// create the fftw plan

		cout << " -->  5 --> create the fftw plan" << endl;

		p = fftw_plan_r2r_2d(N, N,
				     in, out,
				     FFTW_REDFT10, FFTW_REDFT10,
				     FFTW_ESTIMATE);

		// build the input data vector

		cout << " -->  6 --> build the input data vector" << endl;

		#pragma omp parallel default (none) \
				     shared (in)
		{
			#pragma omp for
			for (long i = 0; i < N*N; ++i)
			{
				in[i] = static_cast<double>(i+1);
			}
		}

		// execute the fftw plan

		cout << " -->  7 --> execute the fftw transformation" << endl; 

		t1 = clock();

		fftw_execute(p);

		t2 = clock();

		// report timing

		cout << " -->  time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

		// some output to have an idea

		cout << " -->  8 --> some output" << endl;

		cout << " -->  out[0]     = " << out[0] << endl;
		cout << " -->  out[1]     = " << out[1] << endl;
		cout << " -->  out[2]     = " << out[2] << endl;
		cout << " -->  out[3]     = " << out[3] << endl;
		cout << " -->  out[N*N-1] = " << out[N*N-1] << endl;

		cout << " -->  in[0]     = " << in[0] << endl;
                cout << " -->  in[1]     = " << in[1] << endl;
                cout << " -->  in[2]     = " << in[2] << endl;
                cout << " -->  in[3]     = " << in[3] << endl;
                cout << " -->  in[N*N-1] = " << in[N*N-1] << endl;

		// some output that looks like the Mathematica's output

		const double MAM_FAC = 1/(8*sqrt(N/2.0)*sqrt(N/2.0));

		cout << " -->  9 --> some Mathematica-like output" << endl;

		cout << " -->  out[0]     * MAM_FAC = " << out[0]     * MAM_FAC<< endl;
                cout << " -->  out[1]     * MAM_FAC = " << out[1]     * MAM_FAC << endl;
                cout << " -->  out[2]     * MAM_FAC = " << out[2]     * MAM_FAC << endl;
                cout << " -->  out[3]     * MAM_FAC = " << out[3]     * MAM_FAC << endl;
		cout << " -->  out[N-1]   * MAM_FAC = " << out[N-1]   * MAM_FAC<< endl;
                cout << " -->  out[N]     * MAM_FAC = " << out[N]     * MAM_FAC << endl;
                cout << " -->  out[N+1]   * MAM_FAC = " << out[N+1]   * MAM_FAC << endl;
                cout << " -->  out[N+2]   * MAM_FAC = " << out[N+2]   * MAM_FAC << endl;
		cout << " -->  out[2*N]   * MAM_FAC = " << out[2*N]   * MAM_FAC<< endl;
                cout << " -->  out[2*N+1] * MAM_FAC = " << out[2*N+1] * MAM_FAC << endl;
                cout << " -->  out[2*N+2] * MAM_FAC = " << out[2*N+2] * MAM_FAC << endl;
                cout << " -->  out[2*N+3] * MAM_FAC = " << out[2*N+3] * MAM_FAC << endl;
                cout << " -->  out[N*N-N] * MAM_FAC = " << out[N*N-N] * MAM_FAC << endl;
                cout << " -->  out[N*N-1] * MAM_FAC = " << out[N*N-1] * MAM_FAC << endl;

		// convert the output to a Mathematica-like one

		cout << " --> 10 --> convert the output to a Mathematica-like one" << endl;

		#pragma omp parallel default (none) \
                                     shared (out)
                {
                        #pragma omp for
                        for (long i = 0; i < N*N; ++i)
                        {
                                out[i] = out[i] * MAM_FAC;
                        }
                }


		// create the inverse fftw plan

		cout << " --> 11 --> create the inverse fftw plan" << endl;

		fftw_plan p_inv;

		p_inv = fftw_plan_r2r_2d(N, N, 
		  		         out, in,
				         FFTW_REDFT01, FFTW_REDFT01,
				         FFTW_ESTIMATE);
	
		// execute the inverse fftw plan

		cout << " --> 12 --> execute the fftw inverse-transformation" << endl; 

		t1 = clock();

		fftw_execute(p_inv);

		t2 = clock();

		// report timing

                cout << " -->  time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

		// some output

		cout << " --> 13 --> some output to verify the inverse fftw action" << endl;

		cout << " -->  in[0]     = " << in[0]     << endl;
		cout << " -->  in[1]     = " << in[1]     << endl;
		cout << " -->  in[2]     = " << in[2]     << endl;
		cout << " -->  in[3]     = " << in[3]     << endl;
		cout << " -->  in[N*N-1] = " << in[N*N-1] << endl;

		// some output
		// i divide with in[0] to make in[0] equal to 1
		// so the rest should de equal to the original input

		cout << " --> 15 --> some output to verify the inverse fftw action" << endl;

		cout << " -->  in[0]/in[0]     = " << in[0]/in[0]     << endl;
		cout << " -->  in[1]/in[0]     = " << in[1]/in[0]     << endl;
		cout << " -->  in[2]/in[0]     = " << in[2]/in[0]     << endl;
		cout << " -->  in[3]/in[0]     = " << in[3]/in[0]     << endl;
		cout << " -->  in[N*N-1]/in[0] = " << in[N*N-1]/in[0] << endl;

		// destroy plans

		cout << " --> 16 --> destroy the fftw plans" << endl;

		fftw_destroy_plan(p);
		fftw_destroy_plan(p_inv);

		// clean the fftw threads

		cout << " --> 17 --> cleanup fftw threads" << endl;

		fftw_cleanup_threads();

		// delete the RAM associated with the data containers

		cout << " --> 18 --> delete RAM of input data" << endl;

		delete [] in;

		cout << " --> 19 --> delete RAM of output data" << endl;

		delete [] out;

		cout << " --> 20 --> repeat" << endl;
	}

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

