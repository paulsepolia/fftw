#!/bin/bash

  # 1. compile

  icpc  -O3                \
        -xHost             \
        -Wall              \
        -std=c++11         \
        -wd2012            \
        -static            \
	-parallel          \
	-par-threshold0    \
	driver_program.cpp \
	/opt/fftw/334/intel/lib/libfftw3_omp.a     \
        /opt/fftw/334/intel/lib/libfftw3.a         \
	/opt/fftw/334/intel/lib/libfftw3_threads.a \
	-lm                \
	-openmp            \
	-lpthread          \
        -o x_intel
