
//=============================================================//
//  complex 1d ftw transform				       //	
//  fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE); //                                                
//  openmp version 					       //
//  using "new" to allocate RAM                                //
//=============================================================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <fftw3.h> 

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::clock;

// the main function

int main()
{
	// local variables and parameters 

	const long N = 1.2 * static_cast<long>(pow(10.0, 8.0));
	const int K_MAX = static_cast<int>(pow(10.0, 5.0));
	const int nt = 4;
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << setprecision(20);
	cout << showpos;
	cout << showpoint;

	for (int k = 0; k != K_MAX; ++k)
	{
		cout << "------------------------------------------------------------------->> " << k << endl;

		cout << " -->  1 --> initialize the openmp fftw environment" << endl;

	        fftw_init_threads();

		fftw_complex * in;
		fftw_complex * out;
		fftw_plan p;

		// allocate space

		cout << " -->  2 --> allocate RAM space for input data" << endl;

		in = new fftw_complex [N];

		cout << " -->  3 --> allocate RAM space for output data" << endl;

		out = new fftw_complex [N];

		cout << " -->  4 --> parallelize any subsequent plan" << endl;

		fftw_plan_with_nthreads(nt);

		cout << " -->  5 --> create the fftw plan" << endl;

		p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

		cout << " -->  6 --> build the complex input data vector" << endl;

		// build the "in" complex data item
		// it is "double[N][2]"
		// so the real part is "in[i][0]"
		// and the imaginary part is "in[i][1]"

		// build the real part

		cout << " -->  7 --> build the real part of the complex input data vector" << endl;

		#pragma omp parallel default (none) \
				     shared (in)
		{
			#pragma omp for
			for (long i = 0; i < N; ++i)
			{
				in[i][0] = cos(static_cast<double>(i));
			}
		}

		// build the imaginary part

		cout << " -->  8 --> build the imaginary part of the complex input data vector" << endl;

		#pragma omp parallel default (none) \
				     shared (in)   
		{
			#pragma omp for 
			for (long i = 0; i < N; ++i)
			{
				in[i][1] = sin(static_cast<double>(i));
			}
		}

		// execute the fftw calculation

		cout << " -->  9 --> execute the fftw transformation" << endl; 

		t1 = clock();

		fftw_execute(p);

		t2 = clock();

		cout << " -->  time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

		cout << " --> 10 --> some output" << endl;

		cout << " -->  out[0][0]   = " << out[0][0] << endl;
		cout << " -->  out[1][0]   = " << out[1][0] << endl;
		cout << " -->  out[2][0]   = " << out[2][0] << endl;
		cout << " -->  out[3][0]   = " << out[3][0] << endl;
		cout << " -->  out[N-1][0] = " << out[N-1][0] << endl;
	
		cout << " -->  out[0][1]   = " << out[0][1] << endl;
		cout << " -->  out[1][1]   = " << out[1][1] << endl;
		cout << " -->  out[2][1]   = " << out[2][1] << endl;
		cout << " -->  out[3][1]   = " << out[3][1] << endl;
		cout << " -->  out[N-1][1] = " << out[N-1][1] << endl;
	
		cout << " --> 11 --> destroy plan" << endl;

		fftw_destroy_plan(p);

		cout << " --> 12 --> cleanup fftw threads" << endl;

		fftw_cleanup_threads();

		cout << " --> 13 --> delete RAM of input data" << endl;

		delete [] in;

		cout << " --> 14 --> delete RAM of output data" << endl;

		delete [] out;

		cout << " --> 15 --> repeat" << endl;
	}

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

